# Netflix Watch



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## Vue.js
When running in to build issues, try removing the node_modules cache
```
rm -rf ./node_modules/.cache
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

