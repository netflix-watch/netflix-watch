
const state = {
    menu: [
    {
        title: 'dashboard',
    },
    {
        title: 'campaigns',
    },
    {
        title: 'claims',
    },
    {
        title: 'statistics',
    },
    {
        title: 'account',
    },
    {
        title: 'settings',
    },
    {
        title: 'help'
    }
    ]
}

const getters = {
    listMenu (state) {
      return state.menu
    }
  }

export default {
    state,
    getters
  }