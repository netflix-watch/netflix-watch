import Vue from 'vue'
import { IonicVueRouter } from '@ionic/vue'

// Login
import Dashboard from '@/views/Dashboard';
import NetflixSophie from '@/views/Watchlists/NetflixSophie';
import NetflixClaudia from '@/views/Watchlists/NetflixClaudia';
import NetflixGuido from '@/views/Watchlists/NetflixGuido';
import kitchensink from '@/views/kitchensink';
import moodSwings from '@/views/kitchensink/moodSwings';
import userFilter from '@/views/kitchensink/userFilter';

Vue.use(IonicVueRouter);

export default new IonicVueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/watch/sophie',
      name: 'netflix-watch-sophie',
      component: NetflixSophie
    },
    {
      path: '/watch/claudia',
      name: 'netflix-watch-claudia',
      component: NetflixClaudia
    },
    {
      path: '/watch/guido',
      name: 'netflix-watch-guido',
      component: NetflixGuido
    },
    {
      path: '/kitchensink',
      name: 'kitchensink',
      component: kitchensink
    },
    {
      path: '/kitchensink/moodSwings',
      name: 'moodSwings',
      component: moodSwings
    },
    {
      path: '/kitchensink/filter',
      name: 'userFilter',
      component: userFilter
    }
  ]
});
