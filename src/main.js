import Vue from 'vue'
import App from './App'
import i18n from './i18n'
import router from './router'
import axios from './backend/vue-axios'
import store from './store'
import Ionic from '@ionic/vue';
import Vuelidate from 'vuelidate';

import './assets/scss/main.scss';
// import '@ionic/core/css/core.css';
import '@ionic/core/css/ionic.bundle.css';


Vue.use(Ionic);
Vue.use(Vuelidate);
Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  axios,
  store,
  render: h => h(App)
}).$mount('#app');
