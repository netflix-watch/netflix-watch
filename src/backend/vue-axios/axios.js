import axios from 'axios'

const locationUrl = process.env.NODE_ENV === 'development' ? `app.novik.develop.novik.dev` : window.location.hostname
let locationUrlApi = locationUrl.replace("app.", "api.");

const apiUrl = localStorage.api ? localStorage.api : locationUrlApi

const API_URL = `https://${apiUrl}/api`;

// if changing the name Bearer in Authorization please inform backend
export default axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.token,
  }
})
