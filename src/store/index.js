import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    totalPizzaCount: 10, // The TV inventory
    isJennyHappy: false,
    persons: [
      {
        name: 'Larry',
        happy: false,

      },
      {
        name: 'Amber',
        happy: true,
      },
    ],
    items: [
      {
        id: 1,
        name: 'Tuna',
        checked: false
      },
      {
        id: 2,
        name: 'Hawaii',
        checked: false
      },
      {
        id: 3,
        name: 'Salami',
        checked: false
      },
      {
        id: 4,
        name: 'Shoarma',
        checked: false
      },
    ]
  },

  mutations: {
    // Jenny
    removePizza(state, amount) {
      console.log('Jenny');
      state.totalPizzaCount-= amount
    },
    wrongPizza(state, amount) {
      state.totalPizzaCount += amount
    },
    moodSwingJenny(state) {
      state.isJennyHappy = !state.isJennyHappy
    },
    moodSwings(state, index) {
      Vue.set(state.persons[index], "happy", !state.persons[index].happy);
    },
    itemChecked(state, index) {
      Vue.set(state.items[index], "checked", !state.items[index].checked);
    },
    toggle(state, index) {
      Vue.set(state.persons[index], "isSelected", !state.persons[index].isSelected);
    }
  },

  actions: {
    // Larry
    removePizza(context, amount) {
      console.log('Larry');
      if(context.state.totalPizzaCount >= amount) {
        context.commit('removePizza', amount)
      }
    },
    wrongPizza(context, amount) {
      if(context.state.totalPizzaCount >= amount) {
        context.commit('wrongPizza', amount)
      }
    },
    moodSwingJenny(context, mood) {
      context.commit('moodSwingJenny', mood)
    },
    // moodSwings(context, persons, index) {
    //   context.commit('moodSwings', persons, index);
    // }
  }
})
